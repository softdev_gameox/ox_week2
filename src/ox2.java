import java.io.UncheckedIOException;
import java.util.Scanner;

public class ox2 {
    private static char[][] table = { { '-', '-', '-' }, { '-', '-', '-' }, { '-', '-', '-' } }; // 00 01 02 , 11 12 13
    private static char turn = 'O';
    private static int row;
    private static int col;
    private static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) throws Exception {
        printWelcome();
        do {

            showTable();
            showTurn();
            inputRowCol();
            switchTurn();
            if (isFinish()) {
                break;
            }
        } while (!isFinish());

    }

    private static boolean isFinish() {
        return false;

    }

    private static void printWelcome() {
        System.out.println("Welcome to OX Game");

    }

    private static void showTable() {
        for (int r = 0; r < 3; r++) { // สร้างตาราง
            for (int c = 0; c < 3; c++) {

                System.out.print(table[r][c] + " ");
            }
            System.out.println();
        }

    }

    private static void showTurn() {
        System.out.println("Turn " + turn);
    }

    private static void inputRowCol() {
        System.out.print("Please input row, col: ");
        row = sc.nextInt() - 1;
        col = sc.nextInt() - 1;
        System.out.println("" + row + " " + col);
        table[row][col] = turn;
    }

    private static void switchTurn() {
        if (turn == 'X') {
            turn = 'O';
        } else {
            turn = 'X';
        }

    }
}